use cstr::cstr;
use std::{env, error::Error, ffi::c_void, os::raw::c_char, path::Path};

use libloading::{Library, Symbol};

compromise::register!();

fn main() -> Result<(), Box<dyn Error>> {
    let watch = env::args()
        .nth(1)
        .and_then(|a| a.parse().ok())
        .unwrap_or(false);
    compromise::set_hot_reload_enabled(watch);
    if watch {
        println!("Hot reloading enabled = there will be memory leaks!");
    }

    std::thread::spawn(run).join().unwrap();
    Ok(())
}

fn run() {
    let mut line = String::new();
    let stdin = std::io::stdin();

    println!("Here we go...");

    let n = 3;
    for _ in 0..n {
        load_and_print().unwrap();

        println!("-----------------------------");
        println!("Press Enter to go again, Ctrl-C to exit...");

        line.clear();
        stdin.read_line(&mut line).unwrap();
    }
}

fn load_and_print() -> Result<(), libloading::Error> {
    let lib_path =
        Path::new(&std::env::var_os("HOME").unwrap()).join(".cargo_target_build/debug/libgreet.so");
    unsafe {
        let lib = Library::new(lib_path)?;
        let greet: Symbol<unsafe extern "C" fn(name: *const c_char)> = lib.get(b"greet")?;
        greet(cstr!("reloading").as_ptr());
    }

    Ok(())
}
