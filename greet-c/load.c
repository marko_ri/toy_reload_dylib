#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef void (*greet_t)(const char *name);

int main(void) {
  char *lib_path = strcat(getenv("HOME"), "/.cargo_target_build/debug/libgreet.so");
  void *lib = dlopen(lib_path, RTLD_LAZY);
  // void *lib = dlopen("./libgreet.so", RTLD_LAZY);
  if (!lib) {
    fprintf(stderr, "failed to load library\n");
    return 1;
  }

  greet_t greet = (greet_t) dlsym(lib, "greet");
  if (!greet) {
    fprintf(stderr, "could not look up symbol 'greet'\n");
    return 1;
  }

  greet("venus");
  dlclose(lib);

  return 0;
}
