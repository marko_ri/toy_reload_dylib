use std::{ffi::CStr, os::raw::c_char};

/// # Safety
/// Pointer must be valid, and point to a null-terminated
/// string. What happens otherwise is UB.
#[no_mangle]
pub unsafe extern "C" fn greet(name: *const c_char) {
    let cstr = unsafe { CStr::from_ptr(name) };
    println!("Hello 123, {}!", cstr.to_str().unwrap());
}
