use cstr::cstr;
use std::ffi::c_void;
use std::sync::OnceLock;

pub type NextFn = unsafe extern "C" fn(*mut c_void, *mut c_void, *mut c_void);

static SYSTEM_THREAD_ATEXIT: OnceLock<Option<NextFn>> = OnceLock::new();

fn atexit() -> &'static Option<NextFn> {
    SYSTEM_THREAD_ATEXIT.get_or_init(|| unsafe {
        #[allow(clippy::transmute_ptr_to_ref)]
        let name = cstr!("__cxa_thread_atexit_impl").as_ptr();
        std::mem::transmute(libc::dlsym(
            libc::RTLD_NEXT,
            #[allow(clippy::transmute_ptr_to_ref)]
            name,
        ))
    })
}

/// Turns glibc's TLS destructor register function, `__cxa_thread_atexit_impl`,
/// into a no-op if hot reloading is enabled.
///
/// # Safety
/// This needs to be public for symbol visibility reasons, but you should
/// never need to call this yourself
pub unsafe fn thread_atexit(func: &mut c_void, obj: *mut c_void, dso_symbol: *mut c_void) {
    if crate::is_hot_reload_enabled() {
        // avoid registering TLS destructors on purpose, to avoid
        // double-frees and general crashiness
        println!("HOT_ENABLED");
    } else if let Some(system_thread_atexit) = *atexit() {
        // hot reloading is disabled, and system provides `__cxa_thread_atexit_impl`,
        // so forward the call to it.
        println!("HOT_DISABLED");
        system_thread_atexit(func, obj, dso_symbol);
    } else {
        // hot reloading is disabled *and* we don't have `__cxa_thread_atexit_impl`,
        // throw hands up in the air and leak memory.
    }
}
